﻿using UnityEngine;
using System.Collections;

public class CSCoreLoadMusic : MonoBehaviour {
    public AudioSource ass;
	// Use this for initialization
	void PlayMusic () {
        CSCAudioClip cac = new CSCAudioClip(PlayerPrefs.GetString("musicPath"));
        ass.clip = cac.Clip;
        ass.loop = true;
        ass.Play();
	}
	
	// Update is called once per frame
	void Start () {
        if (PlayerPrefs.GetInt("isSoundCloud")  == 1)
        {
            enabled = (false);
        }else
        {
            PlayMusic();
        }
	}
}
