﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;
using System;
using UnityEngine.UI;
using TagLib;
using System.IO;

public class TheCore : MonoBehaviour {
    [Header("Audio Equalizer Settings")]
    public float[] samples;
    public float offset;
    public Transform parent;
    public RectTransform template;
    public Transform[] straws;
    public AudioSource audioSource;
    public float smaller;
    public float bigger;
    public float smoothTime;
    IntPtr id3v1;
    IntPtr id3v2;
    private IntPtr handle_mpg;
    public enum wins { Rectangular, Triangle, Hamming, Hanning, Blackman, BlackmanHarris};
    public wins windowed;
    public int magnifier;
    public TheCore_Curve curve;
   // public static bool curveModifier;
    public Toggle curveModToggle;
    [Header("Text Settings")]
    public string artist;
    public string songName;
    public string subSongName;
    public Text artistT;
    public Text titleT;
    public Text subLabel;
    public RawImage albumArt;
    public Texture2D coverArt = new Texture2D(2,2, TextureFormat.RGBAFloat, false);
    
    // Use this for initialization
    void Start () {
        magnifier = PlayerPrefs.GetInt("magnifierValue");
        if (PlayerPrefs.GetInt("isSoundCloud") == 0)
        {
            try
            {
                TagLib.File file = TagLib.File.Create(PlayerPrefs.GetString("musicPath"));
                try { artistT.text = file.Tag.FirstPerformer.ToUpper(); } catch (NullReferenceException) { Debug.LogError("Artist Name not found! "); artistT.text = ""; }
                try { titleT.text = file.Tag.Title.ToUpper(); } catch (NullReferenceException) { Debug.LogError("Song Name not found! "); titleT.text = System.IO.Path.GetFileNameWithoutExtension(PlayerPrefs.GetString("musicPath")).ToUpper(); }
                try { subLabel.text = file.Tag.Album.ToUpper(); } catch (NullReferenceException) { Debug.LogError("Album Name not found! "); subLabel.text = ""; }
                try
                {
                    coverArt.LoadImage(file.Tag.Pictures[0].Data.Data);
                    coverArt.mipMapBias = 0f;
                    albumArt.texture = coverArt;

                }
                catch (Exception e)
                {
                    Debug.Log("Welp, that didn't workout.");
                    Debug.Log(e.ToString());
                }
            }
            catch (Exception e)
            {
                Debug.Log(e.ToString());
            }
        }
        
        samples = new float[samples.Length * magnifier];
        straws = new Transform[samples.Length/ magnifier];
        for (int i = 0; i < samples.Length/ magnifier; i++)
        {
            RectTransform newObj = (RectTransform)Instantiate(template, template.transform.position, template.transform.rotation);
            newObj.SetParent(parent);
            newObj.transform.localScale = new Vector3(1, 1, 1);
            Vector3 tempPos = template.localPosition;
            newObj.transform.localPosition = new Vector3(tempPos.x + offset * i, tempPos.y, tempPos.z);
            newObj.name = i + ". Straw";
            //Debug.Log(newObj.localPosition.x + ";" + newObj.localPosition.y + ";" + newObj.localPosition.z);

            straws[i] = newObj.GetComponent<Redirector>().desirer;
        }

        template.gameObject.SetActive(false);
	}

	// Update is called once per frame
	void Update () {
        audioSource.GetSpectrumData(samples, 0, current());
        if(smaller == 0)
        {
            Debug.LogError("Cannot divide by 0! Setting to 1");
            smaller = 1;
        }
        for (int i = 0; i < samples.Length/ magnifier; i++)
	    {
            //Vector3 template2 = straws[i].localPosition;
            float curved = 1;
            if (curveModToggle.isOn)
            {
                curved = curve.values[i];
            }else
            {
                curved = 1;
            }
            straws[i].localPosition = new Vector3(straws[i].localPosition.x,
               template.GetComponent<Redirector>().desirer.localPosition.y + Mathf.Clamp((samples[i] / smaller * bigger) * curved, 0, 375), straws[i].localPosition.z);
            //straws[i].GetComponent<SmoothMovesOneAxis>().smoothTime = smoothTime;
        }
        /*if (Input.GetKeyDown(KeyCode.Escape))
        {
            UnityEngine.SceneManagement.SceneManager.LoadLevel(0);
        }*/

    }

    FFTWindow current()
    {
        switch (windowed)
        {
            case wins.Rectangular:
                return FFTWindow.Rectangular; break;
            case wins.Triangle:
                return FFTWindow.Triangle; break;
            case wins.Hamming:
                return FFTWindow.Hamming; break;
            case wins.Hanning:
                return FFTWindow.Hanning; break;
            case wins.Blackman:
                return FFTWindow.Blackman; break;
            case wins.BlackmanHarris:
                return FFTWindow.BlackmanHarris; break;
            default:
                Debug.Log("FUCK OFF"); return FFTWindow.Blackman; break;
        }
    }
    public void SetWindowBasedOnInt(int value)
    {
        switch (value)
        {
            case 0:
                windowed = wins.Rectangular;
                break;
            case 1:
                windowed = wins.Triangle;
                break;
            case 2:
                windowed = wins.Hamming;
                break;
            case 3:
                windowed = wins.Hanning;
                break;
            case 4:
                windowed = wins.Blackman;
                break;
            case 5:
                windowed = wins.BlackmanHarris;
                break;

        }
    }

    
}
