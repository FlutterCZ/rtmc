﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Linq;

public class UpdateWithData : MonoBehaviour {

    public TheCore tc;
    public string[] converted = new string[64];
	// Update is called once per frame
	void Update () {
        int i = 0;
        foreach(float f in tc.samples)
        {
            if(i <= tc.samples.Length)
            {
                converted[i] = f.ToString();
                i++;
            }
        }
        GetComponent<Text>().text = string.Join("; ", converted);
    }
}
