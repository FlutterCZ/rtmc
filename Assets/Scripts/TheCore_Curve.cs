﻿using UnityEngine;
using System.Collections.Generic;

public class TheCore_Curve : MonoBehaviour {

    public AnimationCurve curve = AnimationCurve.Linear(0, 0, 64, 64);
    public List<float> values = new List<float>();
    // Use this for initialization
    void Start () {
        //Debug.Log(string.Format("Length: {0} ", curve.length));
        for (int i = 0; i < 64; i++)
        {
            //curve.AddKey(i, 0);
            float value = curve.Evaluate((1.0f / 64.0f) * i);
            Debug.Log(string.Format("{0}: {1}", i, value));
            values.Add(value);
        }

    }
	
	// Update is called once per frame
	void Update () {
        
    }
}
