﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class LoadOldSmokeValues : MonoBehaviour {

    public Slider slide;
    public Toggle tog;
	// Use this for initialization
	void Awake () {
        tog.isOn = IntToBool(PlayerPrefs.GetInt("smokeEnabled"));
        slide.value = PlayerPrefs.GetFloat("smokeBirthrate");
	}
	
	// Update is called once per frame
	bool IntToBool (int value) {
	    if(value > 2)
        {
            return false;
            Debug.LogError("int Value too great!");
        }else{
            switch (value)
            {
                case 0:
                    return false;
                case 1:
                    return true;
                default:
                    return false;
            }
        }
	}
}
