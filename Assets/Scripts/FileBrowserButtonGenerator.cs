﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;
using UnityEngine.UI;
public class FileBrowserButtonGenerator : MonoBehaviour {

    public GameObject btnTemplate;
    List<GameObject> btnArray = new List<GameObject>();
    //blic float offset;
    public string currentPath;
    public Color baa;
    public Color baas;
    public float offset;
    public Vector3 test;
    public RectTransform rt;
    // Use this for initialization
    void Start () {
        switch (Application.platform)
        {
            case RuntimePlatform.Android:
                currentPath = "/sdcard/";
                break;
            case RuntimePlatform.WindowsPlayer:
                currentPath = "C:/Users/";
                break;
            case RuntimePlatform.WindowsEditor:
                currentPath = "C:/Users/";
                break;
            case RuntimePlatform.LinuxPlayer:
                currentPath = "/home/";
                break;
        }




        redraw(currentPath);

        GameObject parentBtn = (GameObject)Instantiate(btnTemplate, btnTemplate.transform.position, Quaternion.identity);
        parentBtn.name = "parentFolder";
        parentBtn.transform.localScale = btnTemplate.transform.lossyScale;
        parentBtn.GetComponent<Redirector>().txt.text = ". . .";
        parentBtn.transform.SetParent(this.transform);
        //btnArray.Add(parentBtn);
        parentBtn.GetComponent<Button>().onClick.AddListener(() => { parentFolder(); });
        parentBtn.active = true;
    }
	
	void redraw(string path)
    {
        DirectoryInfo[] folders;
        folders = new DirectoryInfo(path).GetDirectories();

        FileInfo[] files;
        files = new DirectoryInfo(path).GetFiles();

        //Parent directory button
        

        int num = 1; 
        foreach(DirectoryInfo di in folders)
        {
            Vector3 pos = new Vector3(btnTemplate.transform.position.x, 
                btnTemplate.transform.position.y - offset * num, 
                btnTemplate.transform.position.z);
            GameObject newBtn = (GameObject)Instantiate(btnTemplate, pos, Quaternion.identity);
            newBtn.name = "folder";
           // newBtn.GetComponent<Button>().
            newBtn.transform.localScale = btnTemplate.transform.lossyScale;

            newBtn.transform.localPosition = pos;
            newBtn.transform.SetParent(this.transform);
            btnArray.Add(newBtn);
            newBtn.GetComponent<Redirector>().txt.text = di.Name;

            newBtn.GetComponent<Button>().onClick.AddListener(() => { openFolder(newBtn.GetComponent<Redirector>().txt.text); });

            newBtn.active = true;
            num++;
        }

        foreach(FileInfo fi in files)
        {
            Vector3 pos = new Vector3(btnTemplate.transform.position.x,
                btnTemplate.transform.position.y - offset * num,
                btnTemplate.transform.position.z);
            GameObject newBtn = (GameObject)Instantiate(btnTemplate, pos, Quaternion.identity);
            newBtn.name = "file";
            newBtn.transform.localScale = btnTemplate.transform.lossyScale;
            newBtn.transform.localPosition = pos;
            newBtn.transform.SetParent(this.transform);
            btnArray.Add(newBtn);
            newBtn.GetComponent<Redirector>().txt.text = fi.Name;

            newBtn.GetComponent<Button>().onClick.AddListener(() => { openFile(newBtn.GetComponent<Redirector>().txt.text); });
            newBtn.active = true;
            num++;
        }
        btnTemplate.active = false;
    }

    void openFile(string fileName)
    {
        PlayerPrefs.SetString("musicPath", currentPath + "/" + fileName);
        UnityEngine.SceneManagement.SceneManager.LoadScene(1);
        rt.localPosition = Vector3.zero;
    }

    void openFolder(string folder)
    {
        foreach(GameObject go in btnArray)
        {
            Destroy(go);
        }
        currentPath = currentPath + "/" + folder;
        redraw(currentPath);
        rt.localPosition = Vector3.zero;
    }

    void parentFolder()
    {
        foreach (GameObject go in btnArray)
        {
            Destroy(go);
        }
        currentPath = Directory.GetParent(currentPath).FullName;
        redraw(currentPath);
        rt.localPosition = Vector3.zero;
    }

    void Update()
    {
        Vector3 np = new Vector3(0, Mathf.Clamp(rt.localPosition.y, 0, 50000000), 0);
        rt.localPosition = np;

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(0);
        }
    }

}
