﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Text.RegularExpressions;
using LitJson;
using UnityEngine.UI;
using System;

public class LoadThroughSoundCloud : MonoBehaviour {
    public string URL;
    public string clientID;
    public AudioClip clip;
    
    public GameObject identifier;
    public UnityEngine.UI.Text progress;
    public AudioSource source;

    [Header("Metadata things")]
    public string trackID;
    public string artwork_url;
    public string title;
    public string uploader;
    public string label_name;

    [Header("Metadata things")]
    public Text artistT;
    public Text titleT;
    public Text subLabel;
    public RawImage albumArt;
    public Texture2D coverArt = new Texture2D(2, 2, TextureFormat.RGBAFloat, false);

    const string resolveURLBase = "http://api.soundcloud.com/resolve?url=";
    // Use this for initialization
    IEnumerator Start () {
        URL = PlayerPrefs.GetString("scUrl");
        if (PlayerPrefs.GetInt("isSoundCloud") == 1)
        {
            //JSON FETCHER
            identifier.SetActive(true);
            progress.gameObject.SetActive(true);
            WWW fetcher = new WWW(resolveURLBase + URL + "&client_id=" + clientID);
            progress.text = "Metadata downloading...";
            yield return fetcher;

            //JSON EXTRACTOR
            JsonData jd = JsonMapper.ToObject(fetcher.text);
            trackID = jd["id"].ToString();
            artwork_url = jd["artwork_url"].ToString();
            title = jd["title"].ToString().ToUpper();
            uploader = jd["user"]["username"].ToString().ToUpper();
            try { label_name = jd["label_name"].ToString().ToUpper(); } catch (Exception) { };

            artistT.text = uploader;
            titleT.text = title;
            subLabel.text = label_name;

            //IMAGE DOWNLOADER
            WWW img = new WWW(artwork_url);
            yield return img;
            //img.LoadImageIntoTexture(coverArt);
            albumArt.texture = img.texture;

            //STREAM DOWNLOADER
            if (File.Exists(Path.GetTempPath() + "/RTMC/soundcloudTemp.mp3"))
            {
                File.Delete(Path.GetTempPath() + "/RTMC/soundcloudTemp.mp3");
                //no traces pls
            }
            WWW streamURL = new WWW("http://api.soundcloud.com/tracks/" + trackID + "/stream?client_id=" + clientID);
            while (!streamURL.isDone)
            {
                progress.text = (streamURL.progress * 100).ToString("0.0");
                yield return 0;
            }
            progress.gameObject.SetActive(false);
            identifier.SetActive(false);
            Directory.CreateDirectory(Path.GetTempPath() + "/RTMC");
            File.WriteAllBytes(Path.GetTempPath() + "/RTMC/soundcloudTemp.mp3", streamURL.bytes);
            Debug.Log("Downloaded file, storing in TEMP");
            clip = new CSCAudioClip(Path.GetTempPath() + "/RTMC/soundcloudTemp.mp3").Clip;
            source.clip = clip;
            source.Play();
            GetComponent<RawImage>().enabled = false;
            //FINISHED
            
        }else
        {
            this.gameObject.SetActive(false);
        }
    }
}
