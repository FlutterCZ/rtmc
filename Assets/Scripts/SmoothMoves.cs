﻿using UnityEngine;
using System.Collections;

public class SmoothMoves : MonoBehaviour {

    public Transform target;
    public float smoothTime;
    public Vector3 velocity = Vector3.zero;


    void Update () {
        transform.position = Vector3.SmoothDamp(transform.position, target.position, ref velocity, smoothTime);
    }
}
