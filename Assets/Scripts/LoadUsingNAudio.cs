﻿using UnityEngine;
using System.Collections;
using NAudio;
using NAudio.MediaFoundation;
using NAudio.Codecs;
using NAudio.Wave;
using System;
using System.IO;
using NAudio.FileFormats;
public class LoadUsingNAudio : MonoBehaviour
{
    public AudioFileReader aud;
    public AudioClip craftClip;
    private float[] AudioData;
    private float[] readBuffer;
    public AudioSource soundSystem;
    public string musicPath;
    public long length;
    public int currentTime;
   // public UnityEngine.UI.InputField ifs;
    void Start () {
        playMusic();
    }
    public void playMusic()
    {
        //musicPath = ifs.text;
        musicPath = PlayerPrefs.GetString("musicPath");
        try
        {
            //Parse the file with NAudio
            aud = new AudioFileReader(musicPath);
            length = aud.Length/8;
            AudioData = new float[aud.Length];
            aud.Read(AudioData, 0, (int)aud.Length);
            craftClip = AudioClip.Create("name", (int)aud.Length/8, aud.WaveFormat.Channels, aud.WaveFormat.SampleRate, false);
            craftClip.SetData(AudioData, 0);
            soundSystem.clip = craftClip;
            soundSystem.loop = true;
            soundSystem.Play();
            
            //aud.Dispose();
        }catch(Exception boogan)
        {
            Debug.LogError(boogan.ToString());
        }
    }
    void Update()
    {
        currentTime = soundSystem.timeSamples;
        if (Input.GetKeyDown(KeyCode.F10))
        {
            soundSystem.Stop();
            soundSystem.Play();
            //soundSystem.clip = null;
            //playMusic();
        }
        /*if (soundSystem.timeSamples > length)
        {
            soundSystem.Stop();
            soundSystem.Play();
        }*/
    }/**/
    

}




