﻿using UnityEngine;
using System.Collections;

public class SmoothMovesOneAxis : MonoBehaviour {

    public Transform target;
    public float smoothTime = 0.1f;
    public float velocity = 0;


    void Update()
    {
        Vector3 wev = new Vector3(transform.position.x, Mathf.SmoothDamp(transform.position.y, target.position.y, ref velocity, smoothTime), transform.position.z);
        transform.position = wev;
    }
}
