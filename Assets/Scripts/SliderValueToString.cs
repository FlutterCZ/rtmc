﻿using UnityEngine;
using System.Collections;

public class SliderValueToString : MonoBehaviour {
    public void SendString(UnityEngine.UI.Text dest)
    {
        dest.text = gameObject.GetComponent<UnityEngine.UI.Slider>().value.ToString();
    }
}
