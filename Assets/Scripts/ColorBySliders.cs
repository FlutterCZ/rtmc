﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class ColorBySliders : MonoBehaviour {
    public Slider r;
    public Slider g;
    public Slider b;
	// Update is called once per frame
    public void Awake()
    {
        r.value = PlayerPrefs.GetFloat("red")*255;
        g.value = PlayerPrefs.GetFloat("green")*255;
        b.value = PlayerPrefs.GetFloat("blue")*255;
    }

	public void UpdateValues () {
        Color c = new Color(r.value/255, g.value/255, b.value/255);
        GetComponent<Image>().color = c;
	}
}
