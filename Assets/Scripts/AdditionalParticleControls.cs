﻿using UnityEngine;
using System.Collections;

public class AdditionalParticleControls : MonoBehaviour {
    public ParticleSystem ps;
    public TheCore spectrum;
    public float multiplier;

    public float smoothTime = 0.1f;
    public float velocity = 0;

    public float average;
    // Use this for initialization
    void Start () {
        ps = GetComponent<ParticleSystem>();

	}

    // Update is called once per frame
    void Update() {
        ParticleSystem.Particle[] p = new ParticleSystem.Particle[ps.particleCount + 1];
        int l = ps.GetParticles(p);

        //average = avg(spectrum.samples, 8);
        //average bbeet
        float avg = ((spectrum.samples[0] + spectrum.samples[1] + spectrum.samples[2]) + (spectrum.samples[3] + spectrum.samples[4] + spectrum.samples[5]) / 6)* multiplier;


        int i = 0;
        while (i < l)
        {
            p[i].velocity = new Vector3(0, 0, Mathf.SmoothDamp(p[i].velocity.z, avg, ref velocity, smoothTime));
            i++;
        }

        ps.SetParticles(p, l);
    }

    /*float avg (float[] input, int maxVal)
    {
        float addition = 0;
        int pos = 0;
        foreach(float f in input)
        {
            if(pos < maxVal)
            {
                addition += f;
                pos++;
            }
            
        }
        return addition / maxVal;
    }*/
}
