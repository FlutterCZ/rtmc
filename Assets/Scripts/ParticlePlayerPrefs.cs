﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ParticlePlayerPrefs : MonoBehaviour {
    void Awake()
    {
        if(PlayerPrefs.GetInt("smokeEnabled") == 0)
        {
            gameObject.SetActive(false);
        }else
        {
            gameObject.GetComponent<ParticleSystem>().emissionRate = PlayerPrefs.GetFloat("smokeBirthrate");
        }
    }
}
