﻿using UnityEngine;
using System.Collections;

public class ColorManager : MonoBehaviour {

    public GameObject[] straws;
    public Material ps;
    // Use this for initialization
    public TheCore CoreReactor;

    public bool AverageColor;
	void Start () {
        if (AverageColor)
        {
            straws = GameObject.FindGameObjectsWithTag("straw");
            Color av =  averageColor(new Color(PlayerPrefs.GetFloat("red"), PlayerPrefs.GetFloat("green"), PlayerPrefs.GetFloat("blue")));
            foreach (GameObject go in straws)
            {
                go.GetComponent<UnityEngine.UI.RawImage>().color = av;
            }
            ps.SetColor("_TintColor", averageColor(new Color(av.r, av.g, av.b, 0.5f)));
        }
        else
        {
            straws = GameObject.FindGameObjectsWithTag("straw");
            foreach (GameObject go in straws)
            {
                go.GetComponent<UnityEngine.UI.RawImage>().color = new Color(PlayerPrefs.GetFloat("red"), PlayerPrefs.GetFloat("green"), PlayerPrefs.GetFloat("blue"));
            }
            ps.SetColor("_TintColor", new Color(PlayerPrefs.GetFloat("red"), PlayerPrefs.GetFloat("green"), PlayerPrefs.GetFloat("blue"), 0.5f));
        }
    }

    public Color averageColor(Color orig)
    {
        if(CoreReactor.coverArt != null)
        {
            Color[] clrArray = CoreReactor.coverArt.GetPixels();
            float rTotal = 0;
            float gTotal = 0;
            float bTotal = 0;
            foreach (Color c in clrArray)
            {
                rTotal += c.r;
                gTotal += c.g;
                bTotal += c.b;
            }
            Color clr = new Color(rTotal / clrArray.Length,
                bTotal / clrArray.Length,
                gTotal / clrArray.Length);

            return clr;
        }else
        {
            return orig;
        }
    }
}
