﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Windows.Forms;
public class SceneChange : MonoBehaviour {

    public InputField path;
    public Image colorTaker;
    public Toggle smokeEnabler;
    public Slider birthRate;
    public Slider magnifierSlider;
	// Use this for initialization
	void Start () {
	
	}
	

    public void Click()
    {
        /*PlayerPrefs.SetFloat("red", colorTaker.color.r);
        PlayerPrefs.SetFloat("green", colorTaker.color.g);
        PlayerPrefs.SetFloat("blue", colorTaker.color.b);
        PlayerPrefs.SetInt("smokeEnabled", BoolToInt(smokeEnabler.isOn));
        PlayerPrefs.SetFloat("smokeBirthrate", birthRate.value);
        UnityEngine.SceneManagement.SceneManager.LoadScene(2);*/

        OpenFileDialog ofd = new OpenFileDialog();
        ofd.Filter = "Currently supported formats | *.mp3; *.flac; *.wav; *.wave; *.m4a; *.m4b; *.n4p; *.m4v; *.m4r; *.3gp; *.mp4; *.aac; *.wma; *.wmv; *.asf|All files (*.*)|*.*";
        ofd.Multiselect = false;
        ofd.ShowDialog();
        PlayerPrefs.SetString("musicPath", ofd.FileName);
        PlayerPrefs.SetFloat("red", colorTaker.color.r);
        PlayerPrefs.SetFloat("green", colorTaker.color.g);
        PlayerPrefs.SetFloat("blue", colorTaker.color.b);
        PlayerPrefs.SetInt("smokeEnabled", BoolToInt(smokeEnabler.isOn));
        PlayerPrefs.SetFloat("smokeBirthrate", birthRate.value);
        PlayerPrefs.SetInt("magnifierValue", (int)magnifierSlider.value);
        PlayerPrefs.SetInt("isSoundCloud", 0);
        UnityEngine.SceneManagement.SceneManager.LoadScene(1);

    }

    public void loadSoundcloud()
    {
        PlayerPrefs.SetString("scUrl", path.text);
        PlayerPrefs.SetFloat("red", colorTaker.color.r);
        PlayerPrefs.SetFloat("green", colorTaker.color.g);
        PlayerPrefs.SetFloat("blue", colorTaker.color.b);
        PlayerPrefs.SetInt("smokeEnabled", BoolToInt(smokeEnabler.isOn));
        PlayerPrefs.SetFloat("smokeBirthrate", birthRate.value);
        PlayerPrefs.SetInt("magnifierValue", (int)magnifierSlider.value);
        PlayerPrefs.SetInt("isSoundCloud", 1);
        UnityEngine.SceneManagement.SceneManager.LoadScene(1);
    }

    public int BoolToInt(bool boole)
    {
        if (boole)
        {
            return 1;
        }else
        {
            return 0;
        }
    }
}
