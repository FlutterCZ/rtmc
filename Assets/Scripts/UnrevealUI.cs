﻿using UnityEngine;
using System.Collections;

public class UnrevealUI : MonoBehaviour {
    public Vector3 unrevealedSpot;
    public Vector3 revealedSpot;
    public bool revealed = false;
    public float smoothTime;

    public Vector3 desiredPos;

    public Vector3 velocity = Vector3.zero;
    // Use this for initialization
    // Update is called once per frame
    public bool F12 = false;
    void Start()
    {
        //reveal();
        desiredPos = unrevealedSpot;
    }
    void Update () {
        transform.localPosition = Vector3.SmoothDamp(transform.localPosition, desiredPos, ref velocity, smoothTime);
        if (F12 && Input.GetKeyDown(KeyCode.F12))
        {
            reveal();
        }
    }

    public void reveal()
    {
        if (revealed)
        {
            revealed = false;
            desiredPos = unrevealedSpot;
        }else
        {
            revealed = true;
            desiredPos = revealedSpot;
        }
        
    }

    public void summonOthers(int i)
    {
        switch (i)
        {
            case 0:
                GameObject[] gos = GameObject.FindGameObjectsWithTag("uiReveal");
                foreach (GameObject g in gos)
                {
                    g.SendMessage("reveal");
                }
                break;
            case 1:
                GameObject[] gose = GameObject.FindGameObjectsWithTag("uiBtns");
                foreach (GameObject g in gose)
                {
                    g.SendMessage("reveal");
                }
                break;
        }
    }
}
