# README #

RTMC is an acronym for "Real Time Monster Cat". Current features are visualizing music, loading music during runtime (MP3, FLAC, M4A and many more). That's practically all of it.

### What is this repository for? ###

* For Unity Cloud build feature
* Yes, that's it

### How do I get set up? ###

* Just download it, fork it, whatever
* Tested on Unity 5.3.4f1 [Personal]

### Contribution guidelines ###

* Try to include more formats (like SoundCloud downloading etc.)
* Figure out how does the real MonsterCat visualizer works.
### Who do I talk to? ###

* Me, Pavel Duong.